#ifndef USEREDITOR_H
#define USEREDITOR_H

#include <QWidget>
#include <QFileDialog>
#include <database.h>
#include <QDateTime>
#include <QMessageBox>
#include <QPixmap>

#include <userinfo.h>
#include <settings.h>

namespace Ui {
class UserEditor;
}

class UserEditor : public QWidget
{
    Q_OBJECT

public:
    enum Action {Add, Edit};

    explicit UserEditor(UserInfo &user, Settings *options, Database *pdb = 0, QWidget *parent = 0);
    ~UserEditor();


private slots:
    void on_btnComfirm_clicked();

    void on_btnBrowse_clicked();

    void on_lineEditName_textChanged(const QString &arg1);

private:
    Ui::UserEditor *ui;

    UserInfo user;
    Database *db;

    bool checkUserFields();

signals:
    void Done();
};

#endif // USEREDITOR_H
