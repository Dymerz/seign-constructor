#include "usereditor.h"
#include "ui_usereditor.h"



UserEditor::UserEditor(UserInfo &user, Settings *options, Database *pdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserEditor)
{
    ui->setupUi(this);
    this->user= user;
    db = pdb;

    // Remplit les champs
    ui->labelId->setText("ID: " + QString::number(user.id));
    ui->lineEditFolder->setText(user.folder);
    ui->lineEditName->setText(user.name);
    ui->lineEditFunction->setText(user.function);
    ui->lineEditEmail->setText(user.email);
    ui->lineEditPhone1->setText(user.phone1);
    ui->lineEditPhone2->setText(user.phone2);

    // Charge l'image dans le conteneur et la zone de texte.
    //
    QPixmap pix;
    pix.load(user.image),
    pix= pix.scaledToWidth(ui->pictureView->width());
    ui->pictureView->setPixmap(pix);
//    ui->pictureView->setText(QString("<img src='file:///%1' alt='profile' width='350' height='200'/>").arg(user.image));
    ui->lineEditImage->setText(user.image);

    ui->checkBoxActive->setChecked(user.isActive);
    if(!options->getDebugMode())
        ui->checkBoxActive->setVisible(false);
/*
  QPixmap pic;

    pic.loadFromData(myProductModel.myProduct.pictureData,"PNG");

    ui->txtReference->setText(myProductModel.myProduct.reference);

    ui->txtName->setText(myProductModel.myProduct.name);

    ui->lblPicture->setPixmap(pic);
*/
    if(user.isCompany)
    {
        this->setWindowTitle("Company Editor");
    }
    return;
}

UserEditor::~UserEditor()
{
    delete ui;
}

void UserEditor::on_btnComfirm_clicked()
{
    UserInfo *info= new UserInfo();
    info->id            = user.id;
    info->folder        = ui->lineEditFolder->text().toUpper();
    info->filename      = ui->lineEditName->text();
    info->name          = ui->lineEditName->text();
    info->function      = ui->lineEditFunction->text();
    info->email         = ui->lineEditEmail->text();
    info->phone1        = ui->lineEditPhone1->text();
    info->phone2        = ui->lineEditPhone2->text();
    info->image         = ui->lineEditImage->text();
    info->dateEdited    = QDateTime::currentDateTime().toTime_t();
    info->isActive      = ui->checkBoxActive->isChecked();
    info->isCompany     = user.isCompany;

    if(info->id == -1)
    {
        info->dateAdded = QDateTime::currentDateTime().toTime_t();
        info->isActive  = 1;
    }

    // Vérifie la syntaxe des données.
    if(info->checkFields())
    {
        db->setUser(*info);
        emit Done();
        close();
    }else
    {
        qDebug() << info->lastError;
        QMessageBox::warning(this, "Syntax error", info->lastError, QMessageBox::Ok);
    }
    return;
}

// Ouvre l'explorateur de fichier pour séléctionner l'image du profil.
void UserEditor::on_btnBrowse_clicked()
{
    QString file = QFileDialog::getOpenFileName(this,
        tr("Select an user image"), "",
        tr("Image Files (*.png *.jpg *jpeg *.bmp *.tif);;"));

    if(file==QString::null) return;

    // Place le chemin de l'image dans le champ.
    ui->lineEditImage->setText(file);

    // Place l'image dans la vue de prévisualisation.
    ui->pictureView->setText(QString("<img src='file:///%1' alt='profile' width='350' height='auto'/>").arg(file));
    return;
}

void UserEditor::on_lineEditName_textChanged(const QString &arg1)
{
    QString folder;
    QString nameField= ui->lineEditName->text();
    QString name= nameField.split(" ")[0];
    QString firstname= nameField.mid(name.length(), nameField.length()).replace("'", "");

    name= name.trimmed().toUpper();
    firstname= firstname.trimmed().toUpper();
    folder= name.mid(0, 2) + firstname.mid(0, 3);

    ui->lineEditFolder->setText(folder);
    return;
}
