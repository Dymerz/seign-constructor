#include "parameters.h"
#include "ui_parameters.h"

Parameters::Parameters(Database *db, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Parameters)
{
    ui->setupUi(this);
    this->parent= parent;
    this->db= db;


    QString dbPath      = settings.getDBPath();
    bool    debug       = settings.getDebugMode();
    QString userPath    = db->getParameterValue("usersPath");
    QString separator   = db->getParameterValue("separator");
    QString motd        = db->getParameterValue("motd");

    ui->lineEditDB->setText(dbPath);
    ui->checkBoxDebug->setChecked(debug);
    ui->lineEditUser->setText(userPath);
    ui->lineEditVariables->setText(separator);
    ui->plainTextMOTD->setPlainText(motd);
    return;
}

Parameters::~Parameters()
{
    delete ui;
}

// toggle the debug mode.
void Parameters::on_checkBoxDebug_toggled(bool checked)
{
    settings.setDebugMode(checked);
    return;
}

// Open the HTML file.
void Parameters::on_btnOpenHTML_clicked()
{
    QDesktopServices::openUrl(db->getParameterValue("htmlPath"));
    return;
}

// Save all fields into the DB parameters.
void Parameters::on_btnSave_clicked()
{
    QString DBPath      = ui->lineEditDB->text();
    QString userPath    = ui->lineEditUser->text();
    QString separator   = ui->lineEditVariables->text();
    QString motd        = ui->plainTextMOTD->toPlainText();

    if(DBPath!=settings.getDBPath())
    {
        QMessageBox::information(this, "Apply change?", "The application need a restart to apply settings",
                                 QMessageBox::Ok);
    }

    settings.setDBPath(DBPath);
    db->setParameterValue("usersPath", userPath);
    db->setParameterValue("separator", separator);
    db->setParameterValue("motd", motd);

    emit Done();
    close();
    return;
}

// Open the useredit to edit Society info.
void Parameters::on_btnCompany_clicked()
{
    UserInfo company;
    company= db->getCompany(); // Otbien l'utilisateur Copany dans la DB users.

    UserEditor *usrEditor = new UserEditor (company, &settings, db, this);
    usrEditor->setWindowFlags(Qt::Window);
    usrEditor->show();
    return;
}

void Parameters::on_btnBrowseDB_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
        tr("Select the DB"), "",
        tr("Database (*.db)"));

    if(filename != "")
        ui->lineEditDB->setText(filename);
    return;
}

void Parameters::on_btnBrowseUsers_clicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, tr("Select users directory"),
                                                      "",
                                                      QFileDialog::ShowDirsOnly |
                                                      QFileDialog::DontResolveSymlinks);
    if(folder != "")
        ui->lineEditUser->setText(folder);
    return;
}
