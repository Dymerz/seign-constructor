#include "mainwindow.h"
#include "ui_mainwindow.h"


/**
 * @brief Initialisation princiale de l'application, connecte les SLOTS et SIGNAUX des menus et vérifie l'existance d'une backup
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->btnSave->setVisible(false);

    // Évènements des menu
    connect(ui->actionParameters, &QAction::triggered, this, &showParameters);
    connect(ui->actionConnect, &QAction::triggered, this, &connectDB);
    connect(ui->actionDisconnect, &QAction::triggered, this, &disconnectDB);
    connect(ui->actionQuit, &QAction::triggered, this, &quit);

    // Met à jour le statut dynamiquement.
    connect(&database, &Database::StatusChanged, this, &MainWindow::RefreshStatus);

    // Connecte l'application à la base de donnée.
    connectDB();

    if(lastState != Database::Status::Connected)
        return;

    // Load HTML file into restore the latest if needed.
    if(File::exist(QApplication::applicationDirPath() + "/modele.bck.htm") &&
            !File::equal(QApplication::applicationDirPath() + "/modele.bck.htm", database.getParameterValue("htmlPath")))
    {
        QMessageBox::StandardButton reply;
        reply= QMessageBox::information(this, "backup found!", "The application was exited anormally, do you want to restore the latest changes?", QMessageBox::Yes | QMessageBox::No);

     // Witch file would be loaded?
        if(reply == QMessageBox::Yes)
            ui->plainTextEditHtml->setPlainText(File::read(QApplication::applicationDirPath() + "/modele.bck.htm"));
        else
            ui->plainTextEditHtml->setPlainText(File::read(database.getParameterValue("htmlPath")));
    }
    else
        ui->plainTextEditHtml->setPlainText(File::read(database.getParameterValue("htmlPath")));

    new Highlighter(ui->plainTextEditHtml->document());
}

/**
 * @brief Détruit tous les objets hérité et supprime la backup du d'édition HTML.
 */
MainWindow::~MainWindow()
{
    File::remove(QApplication::applicationDirPath() + "/modele.bck.htm");
    delete ui;
}

// *******************************************************
// **********************   EVENTS   *********************
// *******************************************************

// Bouton btnDeleted.
/**
 * @brief Fonction de suppression d'utilisateurs ou de tags
 */
void MainWindow::on_btnDelete_clicked()
{
    int res;
    res= QMessageBox::question(this,tr("Remove user"),tr("Do you want to remove this user?"),QMessageBox::Yes|QMessageBox::No);

    if(res == QMessageBox::No)
        return;

    switch (tabIndex) {
    case 0:
    // Delete selected user
        {
            if(!ui->tableViewUsers->selectionModel()->hasSelection()) // si aucun éléments n'est séléctionné ne rien faire.
                return;

            QItemSelectionModel *items = ui->tableViewUsers->selectionModel();
            int id= items->selectedRows(0).value(0).data().toInt(); // l'id de la séléction.

            UserInfo user= database.getUserByID(id);

            if(!user.isActive)
            {
                QMessageBox::StandardButton reply;
                reply= QMessageBox::warning(this,
                                         "Removing an user definitely", "You attemp to remove definitely this user.\nThis action cannot be undo.",
                                         QMessageBox::Yes | QMessageBox::Cancel);
                if(reply == QMessageBox::Yes)
                    database.removeUserByID(user.id);

            }else
                database.changeUserState(id, 0);

            refreshUsersList();
        }

        break;
    case 1:
    // Delete selected tag
        {
            if(!ui->tableViewTags->selectionModel()->hasSelection()) // si aucun éléments n'est séléctionné ne rien faire.
                return;

            QItemSelectionModel *items = ui->tableViewTags->selectionModel();
            int id= items->selectedRows(0).value(0).data().toInt(); // l'id de la séléction.

            TagInfo tag= database.getTagByID(id);

            if(!tag.isActive)
            {
                QMessageBox::StandardButton reply;
                reply= QMessageBox::information(this,
                                         "Remove user", "You attemp to remove definitly this tag.\nThis action cannot be undo.",
                                         QMessageBox::Yes | QMessageBox::Cancel);
                if(reply == QMessageBox::Yes)
                    database.removeTagByID(tag.id);

            }else
                database.changeTagState(id, 0);
            refreshTagsList();
        }
        break;
    }
    return;
}
// Bouton btnEdit.
void MainWindow::on_btnEdit_clicked()
{
    switch (tabIndex) {
    case 0:
        // Edit selected user
        {
            if(!ui->tableViewUsers->selectionModel()->hasSelection()) // si aucun éléments n'est séléctionné ne rien faire.
                return;

            QItemSelectionModel *items = ui->tableViewUsers->selectionModel();
            int id= items->selectedRows(0).value(0).data().toInt(); // l'id de la séléction.

            UserInfo user;
            user= database.getUserByID(id); // Obtien toutes les informations à partir de la DB.

            UserEditor *usrEditor = new UserEditor (user, &options, &database, this);
            usrEditor->setWindowFlags(Qt::Window);
            connect(usrEditor, &UserEditor::Done, this, &MainWindow::refreshUsersList);
            usrEditor->show();
        }
        break;
    case 1:
        // Edit selected tag
        {
            if(!ui->tableViewTags->selectionModel()->hasSelection()) // si aucun éléments n'est séléctionné ne rien faire.
                return;

            QItemSelectionModel *items = ui->tableViewTags->selectionModel();
            int id= items->selectedRows(0).value(0).data().toInt(); // l'id de la séléction.

            TagInfo tag;
            tag= database.getTagByID(id);

            TagEditor *tagEditor= new TagEditor(tag, &database, this);
            tagEditor->setWindowFlags(Qt::Window);
            connect(tagEditor, &TagEditor::Done, this, &MainWindow::refreshTagsList);
            tagEditor->show();
        }
        break;
    }
    return;
}
// Bouton btnAdd.
void MainWindow::on_btnAdd_clicked()
{
    switch (tabIndex) {
    case 0:
    // Add selected user
        {
            UserInfo user;
            user.isActive= 1;
            UserEditor *usrEditor = new UserEditor (user, &options, &database, this);
            usrEditor->setWindowFlags(Qt::Window);
            connect(usrEditor, &UserEditor::Done, this, &MainWindow::refreshUsersList);
            usrEditor->show();
        }
        break;
    case 1:
    // Add selected tag
        {
            TagInfo tag;
            tag.isActive= 1;
            TagEditor *tagEditor = new TagEditor (tag, &database, this);
            tagEditor->setWindowFlags(Qt::Window);
            connect(tagEditor, &TagEditor::Done, this, &MainWindow::refreshTagsList);
            tagEditor->show();
        }
        break;
    }
    return;
}
// Sauvegarde le fichier HTML.
void MainWindow::on_btnSave_clicked()
{
    File::write(database.getParameterValue("htmlPath"), ui->plainTextEditHtml->toPlainText());
    ui->btnSave->setEnabled(false);

    // supprime le fichier temporaire.
    if(!File::remove(QApplication::applicationDirPath() + "/modele.bck.htm"))
        QMessageBox::critical(this, "Removing file error", "Une erreur est survenue lors de la suppression du fichier temporaire !", QMessageBox::Ok);

    return;
}
// Génère les fichiers utilisateurs.
void MainWindow::on_btnGenerate_clicked()
{
    // Initialise l'objet Database.
    Generate *gen= new Generate(this, &database, &options, this);

    // Génère les fichiers utilisateurs dans le répertoire par défaut.
    gen->start(database.getUsersList());
    return;
}
// Affiche un aperçus.
void MainWindow::on_btnPreview_clicked()
{
    Generate gen(this, &database, &options);

    QList<QStringList> users= database.getUsersList(); // liste complète des utilisateurs
    QItemSelectionModel *items = ui->tableViewUsers->selectionModel(); // l'objet séléctionné dans la vue des utilisateurs.
    int id= items->selectedRows(0).value(0).data().toInt(); // l'id de l'utilisateur créé.

    // filtre la liste des utilisateurs pour ne contenir que l'utilisateur séléctionné.
    foreach(QStringList user, users)
        if(user[0] != QString::number(id))
            users.removeOne(user);

    // Génère le fichier de l'utilisateur.
    gen.start(users, dir.path());

    // Ouvre le fichier génére avec l'application par défaut.
    QDesktopServices::openUrl(QUrl::fromLocalFile(QString("%1/%2/%3.htm").arg(dir.path()).arg(users[0][1]).arg(users[0][3])));
    return;
}
// User TabWidget.
void MainWindow::on_tabWidget_tabBarClicked(int index)
{
    // Active/Désactive certains bouton en fonction de l'onglet séléctionné.

    ui->btnEdit->setEnabled(false);
    ui->btnDelete->setEnabled(false);

    // Affiche ou cache les bouton en fonction de la page séléctionnée.
    if(index==2) { // && lastState!=(Database::Status::Error | Database::Status::ConnectinError | Database::Status::PathError)
        ui->btnAdd->setVisible(false);
        ui->btnEdit->setVisible(false);
        ui->btnDelete->setVisible(false);
        ui->btnSave->setVisible(true);
    }else{
        ui->btnAdd->setVisible(true);
        ui->btnEdit->setVisible(true);
        ui->btnDelete->setVisible(true);
        ui->btnSave->setVisible(false);
        ui->btnSave->setEnabled(false);

        // Ask for save change if needed.
        if(File::exist(QApplication::applicationDirPath() + "/modele.bck.htm") && !File::equal(database.getParameterValue("htmlPath"), QApplication::applicationDirPath() + "/modele.bck.htm")) {
            QMessageBox::StandardButton reply;
            reply= QMessageBox::warning(this, "Save?", "Some change has been done, do you want to save?", QMessageBox::Yes | QMessageBox::No);

            if(reply == QMessageBox::Yes)
                File::write(database.getParameterValue("htmlPath"), ui->plainTextEditHtml->toPlainText());

            File::remove(QApplication::applicationDirPath() + "/modele.bck.htm");
        }
    }
    tabIndex= index;
}
// User ListView.
void MainWindow::on_tableViewUsers_clicked(const QModelIndex &index)
{
    if(index.isValid()) {
        ui->btnDelete->setEnabled(true);
        ui->btnEdit->setEnabled(true);
    }else{
        ui->btnDelete->setEnabled(false);
        ui->btnEdit->setEnabled(false);
    }
    return;
}
void MainWindow::on_tableViewUsers_doubleClicked(const QModelIndex &index)
{
    emit on_btnEdit_clicked();
    return;
}
// Tag ListView.
void MainWindow::on_tableViewTags_clicked(const QModelIndex &index)
{
    if(index.isValid()) {
        ui->btnDelete->setEnabled(true);
        ui->btnEdit->setEnabled(true);
    }else{
        ui->btnDelete->setEnabled(false);
        ui->btnEdit->setEnabled(false);
    }
    return;
}
void MainWindow::on_tableViewTags_doubleClicked(const QModelIndex &index)
{
    emit on_btnEdit_clicked();
    return;
}
// Editeur HTML.
void MainWindow::on_plainTextEditHtml_textChanged()
{
    TagInfo tag;
    tag.mixed= ui->plainTextEditHtml->toPlainText();

    ui->listWidgetHtmlTags->clear();
    ui->listWidgetHtmlTags->addItems(
                tag.extractTagsFromText(tag.mixed, database.getParameterValue("separator"))
            );

    // if contains somme restricted words.
    if(!tag.checkRestrictedWord())
    {
        QMessageBox::warning(this, "Error parsing file", "Reserved character found!", QMessageBox::Ok);
        ui->btnSave->setEnabled(false);
    }
    else
    {
        File::write(QApplication::applicationDirPath(), "modele.bck", ui->plainTextEditHtml->toPlainText(), false);
    }

    if(lastState == Database::Status::Connected)
        ui->btnSave->setEnabled(true);
    return;
}
// Liste des tags dans la HTML.
void MainWindow::on_listWidgetHtmlTags_currentTextChanged(const QString &currentText)
{
    QString separator= database.getParameterValue("separator");
    int start= ui->plainTextEditHtml->toPlainText().indexOf(QString("%1%2%1").arg(separator).arg(currentText));
    int stop= currentText.length()+2 + start;
    QTextCursor c = ui->plainTextEditHtml->textCursor();
    c.setPosition(start);
    c.setPosition(stop, QTextCursor::KeepAnchor);
    ui->plainTextEditHtml->setTextCursor(c);
    ui->plainTextEditHtml->setFocus();
    return;
}
// Menu Connect.
void MainWindow::on_actionConnect_triggered()
{
    connectDB();
}
// Menu disconnect.
void MainWindow::on_actionDisconnect_triggered()
{
    disconnectDB();
}
// Affiche la fenêtre des paramètres.
void MainWindow::showParameters()
{
    Parameters *params = new Parameters (&database);
    connect(params, &Parameters::Done, this, &MainWindow::refreshUsersList);
    connect(params, &Parameters::Done, this, &MainWindow::refreshTagsList);
    params->setWindowFlags(Qt::Window);
    params->show();
    return;
}
// Affiche la fenêtre "à propos".
void MainWindow::on_actionAbout_triggered()
{
    About *about= new About(this);
    about->setWindowFlags(Qt::Window);
    about->show();
    return;
}
// Ferme l'application proprement.
void MainWindow::quit()
{
    disconnectDB();
    qApp->exit(0);
    return;
}


// *******************************************************
// *****************   CUSTOM FUNCTION   *****************
// *******************************************************

// Rafraichit la vue utilisateurs.
void MainWindow::refreshUsersList()
{
    ui->tableViewUsers->setModel(database.getUsersModel(options.getDebugMode()));
    ui->tableViewUsers->resizeColumnsToContents();
    return;
}
// Rafraichit la vue tags.
void MainWindow::refreshTagsList()
{
    ui->tableViewTags->setModel(database.getTagsModel(options.getDebugMode()));
    ui->tableViewTags->resizeColumnsToContents();
    return;
}
// Démarre la connexion avec la DB
void MainWindow::connectDB()
{
    database.startConnect();

    if(!database.isValid())
        return;

    ui->tableViewUsers->setModel(database.getUsersModel(options.getDebugMode()));
    ui->tableViewUsers->setAlternatingRowColors(true);
    ui->tableViewUsers->resizeColumnsToContents();
    ui->tableViewUsers->setSelectionBehavior(QTableView::SelectRows);
    ui->tableViewUsers->verticalHeader()->setVisible(false);

    ui->tableViewTags->setModel(database.getTagsModel(options.getDebugMode()));
    ui->tableViewTags->setAlternatingRowColors(true);
    ui->tableViewTags->resizeColumnsToContents();
    ui->tableViewTags->setSelectionBehavior(QTableView::SelectRows);
    ui->tableViewTags->verticalHeader()->setVisible(false);
    return;
}
// Supprime la connexion avec la DB.
void MainWindow::disconnectDB()
{
    database.clean();
    delete ui->tableViewUsers->model();
    delete ui->tableViewTags->model();

    return;
}


// *******************************************************
// *********************   SIGNALS   *********************
// *******************************************************

// Rafraichît le statut de la base de donnée.
void MainWindow::RefreshStatus(Database::Status state)
{
    QMetaEnum metaEnum = QMetaEnum::fromType<Database::Status>();
    QString val= metaEnum.valueToKey(state);

    ui->labelStatus->setText(QString("Status: %1").arg(val));

    if(state != Database::Status::Connected)
    {
        ui->tabWidget->setEnabled(false);
        ui->btnGenerate->setEnabled(false);
        ui->btnPreview->setEnabled(false);
        ui->btnSave->setEnabled(false);
        ui->btnAdd->setEnabled(false);
    }

    if(state == Database::Status::Connected)
    {
        ui->tabWidget->setEnabled(true);
        ui->btnGenerate->setEnabled(true);
        ui->btnPreview->setEnabled(true);
        ui->btnSave->setEnabled(true);
        ui->btnAdd->setEnabled(true);
    }

    if(state == Database::Status::BadToken)
    {
        QMessageBox::critical(this, "Database error", "The specified database is not allowed (bad key).", QMessageBox::Ok);
    }

    if(state == Database::Status::PathError)
    {
        QMessageBox::critical(this, "Database error", "An error has occured when attemp to connect to the Database, please check the database path in Options->Parameters.\n" + database.lastError, QMessageBox::Ok);
    }

    if(state == Database::Status::Error && lastState != Database::Status::Error && database.lastError.trimmed()!="")
    {
        QMessageBox::critical(this, "Database error", "Database entering in Error mode.\nCause: " + database.lastError, QMessageBox::Ok);
    }

    lastState= state;
    return;
}
