#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlQueryModel>
#include <QProgressDialog>
#include <QList>
#include <QTextCursor>
#include <QDesktopServices>
#include <QTemporaryDir>

#include <usereditor.h>
#include <tageditor.h>
#include <parameters.h>
#include <userinfo.h>
#include <about.h>

#include <database.h>
#include <settings.h>
#include <file.h>
#include <generate.h>
#include <highlighter.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Database database;
    Database::Status status= Database::Unknown;

private slots:
    void on_btnEdit_clicked();
    void on_btnAdd_clicked();
    void on_btnDelete_clicked();

    void on_tabWidget_tabBarClicked(int index);

    void on_tableViewUsers_clicked(const QModelIndex &index);
    void on_tableViewUsers_doubleClicked(const QModelIndex &index);
    void on_tableViewTags_clicked(const QModelIndex &index);
    void on_tableViewTags_doubleClicked(const QModelIndex &index);

public slots:
    void refreshUsersList();
    void refreshTagsList();

private:
    Ui::MainWindow *ui;
    Settings options; // Class Settings.

    int tabIndex= 0; // l'index de l'onglet séléctionné.
    QTemporaryDir dir; // Dossier temporaire.
    Database::Status lastState; // le dernier status de la DB.

private slots:
    void showParameters();
    void quit();
    void connectDB();
    void disconnectDB();
    void RefreshStatus(Database::Status state);
    void on_btnGenerate_clicked();
    void on_plainTextEditHtml_textChanged();
    void on_listWidgetHtmlTags_currentTextChanged(const QString &currentText);
    void on_actionAbout_triggered();
    void on_btnSave_clicked();
    void on_btnPreview_clicked();
    void on_actionConnect_triggered();
    void on_actionDisconnect_triggered();
};

#endif // MAINWINDOW_H
