#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QWidget>
#include <QDesktopServices>
#include <QFileDialog>
#include <QProcess>

#include <mainwindow.h>
#include <usereditor.h>
#include <settings.h>
#include <database.h>

namespace Ui {
class Parameters;
}

class Parameters : public QWidget
{
    Q_OBJECT

public:
    explicit Parameters(Database *db, QWidget *parent = 0);
    ~Parameters();

private slots:
    void on_btnSave_clicked();


    void on_btnCompany_clicked();

    void on_checkBoxDebug_toggled(bool checked);

    void on_btnOpenHTML_clicked();

    void on_btnBrowseDB_clicked();

    void on_btnBrowseUsers_clicked();

private:
    QWidget *parent;
    Ui::Parameters *ui;
    Settings settings;
    Database *db;

signals:
    void Done();
};

#endif // PARAMETERS_H
