#ifndef TAGEDITOR_H
#define TAGEDITOR_H

#include <QWidget>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QLineEdit>
#include <QMessageBox>
#include <QListWidgetItem>

#include <mylineedit.h>
#include <database.h>
#include <taginfo.h>

namespace Ui {
class TagEditor;
}

class TagEditor : public QWidget
{
    Q_OBJECT

public:
    explicit TagEditor(TagInfo &tag, Database *pdb = 0, QWidget *parent = 0);
    ~TagEditor();

private:
    Ui::TagEditor *ui;

    MyLineEdit *lineeditMixed;
    TagInfo tag;
    QString separator;

    Database *db;

signals:
    void Done();

private slots:
    void on_btnComfirm_clicked();
    void on_btnCancel_clicked();
    void on_listWidgetOther_itemDoubleClicked(QListWidgetItem *item);
    void on_listWidgetCompany_itemDoubleClicked(QListWidgetItem *item);
    void on_listWidgetDB_itemDoubleClicked(QListWidgetItem *item);
};

#endif // TAGEDITOR_H
