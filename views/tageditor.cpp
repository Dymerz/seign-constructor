#include "tageditor.h"
#include "ui_tageditor.h"

TagEditor::TagEditor(TagInfo &tag, Database *pdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TagEditor)
{
    lineeditMixed= new MyLineEdit(pdb, this);
    lineeditMixed->setFont(QFont("MS Shell Dlg 2", 12));

    ui->setupUi(this);
    this->tag= tag;
    this->db= pdb;

    separator= pdb->getParameterValue("separator"); // Obtien le caractère de séparation des variables.

    ui->labelId->setText("ID: "+QString::number(tag.id));
    ui->lineEditTag->setText(separator+tag.value+separator);
    ui->horizontalLayout_4->addWidget(lineeditMixed);
    lineeditMixed->setText(tag.mixed);


    ui->listWidgetDB->setDragDropMode(QAbstractItemView::InternalMove);
    ui->listWidgetCompany->setDragDropMode(QAbstractItemView::InternalMove);
    ui->listWidgetOther->setDragDropMode(QAbstractItemView::InternalMove);


    // Ajoute les tags "Autres" à la liste correspondante.
    ui->listWidgetOther->addItems(tag.others);

    // Ajoute les tags "Database" à la liste correspondante.
    QList<QString> cols= pdb->getColsName();
    ui->listWidgetDB->addItems(cols);

    // Ajoute les tags "Company" à la liste correspondante.
    QList<QString> company= pdb->getCompanyTags(separator);
    ui->listWidgetCompany->addItems(company);

    // Désactive le champ du tag si il est en edition.
    if(tag.id != -1)
        ui->lineEditTag->setEnabled(false);
    return;
}

TagEditor::~TagEditor()
{
    delete ui;
}


void TagEditor::on_btnComfirm_clicked()
{
    tag.value = ui->lineEditTag->text();
    tag.mixed= lineeditMixed->text();
    tag.dateEdited= QDateTime::currentDateTime().toTime_t();
    tag.isActive= ui->checkBoxActive->isChecked();

    QList<QString> allTags;
    allTags.append(db->getColsName());
    allTags.append(db->getCompanyTags(separator));
    allTags.append(tag.others);

    // Si nouveau tag, définir la date d'ajout et le status actif.
    if(tag.id==-1)
    {
        foreach(QList<QString> raw, db->getTags())
        {
            allTags.append(raw[1]);
        }

        tag.dateAdded= QDateTime::currentDateTime().toTime_t();
    }


    // Vérifie la syntaxe du tag entré.
    if(!tag.checkTagSyntax(separator))
    {
        qDebug() << "Tag syntax error or uplicate name";
        QMessageBox::warning(this, "Syntax error", "Tag syntax cannot be parsed", QMessageBox::Ok);
        return;
    }
    tag.value= tag.extractTagsFromText(ui->lineEditTag->text(), separator)[0]; // if correct syntax write it to value.

    // Vérifie lors de l'ajout si le tag n'existe pas déjà.
    if(tag.id==-1 && tag.tagsExist(QStringList(tag.value), allTags))
    {
        qDebug() << "Duplicate tag name";
        QMessageBox::warning(this, "Syntax error", "Duplicate tag name", QMessageBox::Ok);
        return;
    }

    if(!tag.checkMixedSyntax(separator, allTags))
    {
        qDebug() << "Mixed syntax error";
        QMessageBox::warning(this, "Syntax error", "Mixed field cannot be parsed or validated", QMessageBox::Ok);
        return;
    }

    if(!tag.checkRestrictedWord()) {
        qDebug() << "Reserved word found (<TAG>,</TAG>).";
        QMessageBox::warning(this, "Syntax error", "Reserved word found (<TAG>,</TAG>).", QMessageBox::Ok);
        return;
    }

    tag.prepareToDB(separator);
    db->setTag(tag);
    emit Done(); // Émets un signal que la form a fini.
    close();

    return;
}

void TagEditor::on_btnCancel_clicked()
{
    close();
    return;
}

void TagEditor::on_listWidgetOther_itemDoubleClicked(QListWidgetItem *item)
{
    lineeditMixed->insert(separator + item->text() + separator);
    return;
}

void TagEditor::on_listWidgetCompany_itemDoubleClicked(QListWidgetItem *item)
{
    lineeditMixed->insert(separator + item->text() + separator );
    return;
}

void TagEditor::on_listWidgetDB_itemDoubleClicked(QListWidgetItem *item)
{
    lineeditMixed->insert(separator + item->text() + separator );
    return;
}
