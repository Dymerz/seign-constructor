#ifndef MYLINEEDIT_H
#define MYLINEEDIT_H

#include <QLineEdit>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QStandardItemModel>
#include <QMimeData>
#include <QDebug>

#include <database.h>

class MyLineEdit : public QLineEdit
{
public:
    MyLineEdit(Database *db, QWidget *parent);

    void dragEnterEvent(QDragEnterEvent *events);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *parent);

private:
    QString separator;
};

#endif // MYLINEEDIT_H
