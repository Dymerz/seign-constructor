#ifndef TAGINFO_H
#define TAGINFO_H

#include <QString>
#include <QDebug>

class TagInfo
{
public:
    TagInfo();

    int id          = -1;
    QString value   = "";
    QString mixed   = "";
    int dateAdded   = 0;
    int dateEdited  = 0;
    int dateRemoved = 0;
    int isActive    = 0;

    QList<QString> extractTagsFromText(QString text, QString separator);
    void prepareToDB(QString separator);
    bool checkTagSyntax(QString separator);
    bool checkMixedSyntax(QString separator,  QStringList tagsList);
    bool checkRestrictedWord();
    bool tagsExist(QStringList search, QStringList content);

    QStringList others;
private:
    bool isTagFormated= false;
    bool isMixedFormated= false;
};

#endif // TAGINFO_H
