#include "database.h"

Database::Database(QObject *parent) : QObject(parent)
{}

// Appelé lors de la destruction de l'objet.
Database::~Database() {
    clean();
    emit StatusChanged(Status::Disconnected);
}
// Lance la connexion.
/**
 * @brief Démarre le connexion à la base de donnée.
 */
void Database::startConnect()
{
    QFileInfo dbFile(file.getDBPath()); // Obtien les detail sur l'eplacement de la DB.

    // Vérifie si le chemin est un fichier et son existance.
    if(!dbFile.isFile() || !dbFile.exists()) {
        lastError= db.lastError().text();
        DEBUGINFO + db.lastError().text();
        emit StatusChanged(Status::PathError);
        return;
    }

    db = QSqlDatabase::addDatabase("QSQLITE"); // Type de connexion SQLITE.
    db.setDatabaseName(dbFile.filePath()); // Emplacement de la DB.

    // Ouvre la connexion avec la DB.
    if(!db.open()) {
        lastError= db.lastError().text();
        DEBUGINFO + db.lastError().text();
        emit StatusChanged(Status::ConnectinError);
        return;
    }

    query= new QSqlQuery(db);
    emit StatusChanged(Status::Connected);
}
// Vide la connexion.
void Database::clean()
{
    db.close();
    emit StatusChanged(Status::Disconnected);
    return;
}

bool Database::isValid()
{
    if(!db.isOpen()) return false;

    query->prepare("SELECT value FROM parameters WHERE name='key'");
    if(!query->exec())
    {
        lastError= db.lastError().text();
        DEBUGINFO + db.lastError().text();
        emit StatusChanged(Status::BadToken);
        return false;
    }

    query->first();
    if(query->value(0).toString()=="58f82b90c8c2e1f94824ddca3fe433ac")
        return true;

    emit StatusChanged(Status::BadToken);
    return false;
}


/**********************************************/
/************** USER MANAGEMENT ***************/
/**********************************************/

// Obtien sous forme de modèle la liste des utilisateurs visible.
QSqlQueryModel *Database::getUsersModel(bool debug)
{
    if(!db.isOpen()) return new QSqlQueryModel;

    emit StatusChanged(Status::Refreshing);
    QSqlQueryModel *queryModel = new QSqlQueryModel;
    QString queryString= "";

    if(debug) {
        // Requête de Debug.
        queryString=  "SELECT "
                      "	id AS 'Id',"
                      "	folder AS 'Folder',"
                      "	filename AS 'Filename',"
                      "	name AS 'Name',"
                      "	function AS 'Function',"
                      "	email AS 'Email', "
                      "	phone1 AS 'Phone 1',"
                      "	phone2 AS 'Phone 2',"
                      "	image AS 'Image',"
                      "	datetime(dateAdded, 'unixepoch') AS 'Added',"
                      "	datetime(dateEdited, 'unixepoch') AS 'Edited',"
                      "	datetime(dateRemoved, 'unixepoch') AS 'Removed',"
                      " isActive AS 'is Active' "
                      "FROM users WHERE isCompany=0; "
                      "ORDER BY id;";
    }else{
        // Requête par défaut.
        queryString=  "SELECT "
                      "	id AS 'Id',"
                      "	folder AS 'Folder',"
                      "	filename AS 'Filename',"
                      "	name AS 'Name',"
                      "	function AS 'Function',"
                      "	email AS 'Email', "
                      "	phone1 AS 'Phone 1',"
                      "	phone2 AS 'Phone 2'"
                      "FROM users WHERE isCompany=0 AND isActive=1 "
                      "ORDER BY id;";
    }

    // Execute la requête
    if(!query->exec(queryString)) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return queryModel;
    }
    queryModel->setQuery(*query);
    emit StatusChanged(Status::Connected);
    return queryModel;
}
// Obtien un utilisateur à partir de son ID.
UserInfo Database::getUserByID(int id)
{
    UserInfo user;
    if(!db.isOpen()) return user;

    query->prepare("SELECT * FROM users WHERE id=:id "
                   "ORDER BY id;");
    query->bindValue(":id", id);

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return user;
    }

    query->first();

    user.id         = query->value(0).toInt();
    user.folder     = query->value(1).toString();
    user.filename   = query->value(2).toString();
    user.name       = query->value(3).toString();
    user.function   = query->value(4).toString();
    user.email      = query->value(5).toString();
    user.phone1     = query->value(6).toString();
    user.phone2     = query->value(7).toString();
    user.image      = query->value(8).toString();
    user.dateAdded  = query->value(9).toInt();
    user.dateEdited = query->value(10).toInt();
    user.dateRemoved= query->value(11).toInt();
    user.isCompany  = query->value(12).toInt();
    user.isActive   = query->value(13).toInt();

    return user;
}
// Retourne une liste de tous les Users au format QList<QString>
QList<QStringList> Database::getUsersList()
{
    if(!db.isOpen()) return QList<QStringList>();
    emit StatusChanged(Status::Refreshing);
    QList<QStringList> users;
    query->prepare("SELECT * FROM users WHERE isActive=1 AND isCompany=0 "
                   "ORDER BY id;");

    if(!query->exec())
    {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return users;
    }

    while(query->next()) {
        QList<QString> user;

        for(int i= 0; i< query->record().count(); i++) {
            user.append(query->value(i).toString());
        }
        users.append(user);
    }
    emit StatusChanged(Status::Connected);
    return users;
}
// Crée ou modifie un utilisateur.
void Database::setUser(UserInfo user)
{
    if(!db.isOpen()) return;

    // Ajoute un nouvel enregistrement.
    if(user.id==-1) {
        qDebug() << "Create";
        query->prepare("INSERT INTO users ("
                      "folder,filename,name,function,email,phone1,phone2,image,dateAdded,dateEdited,isCompany,isActive"
                      ")VALUES ("
                      ":folder,"
                      ":filename,"
                      ":name,"
                      ":function,"
                      ":email,"
                      ":phone1,"
                      ":phone2,"
                      ":image,"
                      ":dateAdded,"
                      ":dateEdited,"
                      ":isCompany,"
                      ":isActive"
                      ");"
                     );
    }else{
        qDebug() << "Edit";
    // Edite un enregistrement existant.
        query->prepare("UPDATE users SET "
                       " folder=:folder,"
                       " filename=:filename,"
                       " name=:name,"
                       " function=:function,"
                       " email=:email,"
                       " phone1=:phone1,"
                       " phone2=:phone2,"
                       " image=:image,"
                       " dateEdited=:dateEdited,"
                       " isActive=:isActive,"
                       " isCompany=:isCompany "
                       "WHERE id=:id;"
                     );
    }


    // Rempli les champs avec les données.
    query->bindValue(":folder",     user.folder);
    query->bindValue(":filename",   user.filename);
    query->bindValue(":name",       user.name);
    query->bindValue(":function",   user.function);
    query->bindValue(":email",      user.email);
    query->bindValue(":phone1",     user.phone1);
    query->bindValue(":phone2",     user.phone2);
    query->bindValue(":image",      user.image);
    query->bindValue(":dateAdded",  user.dateAdded);
    query->bindValue(":dateEdited", user.dateEdited);
    query->bindValue(":isActive",   user.isActive);
    query->bindValue(":isCompany",  user.isCompany);
    query->bindValue(":id",         user.id);

    // Execute la requête.
    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
    }
    return;
}
// Change l'état d'un utilisateur.
void Database::changeUserState(int id, int state)
{
    if(!db.isOpen()) return;

    query->prepare("UPDATE users SET "
                   " isActive=:state, "
                   " dateRemoved=:dateRemoved "
                   "WHERE id=:id;"
                 );

    query->bindValue(":id", id);
    query->bindValue(":dateRemoved", QDateTime::currentDateTime().toTime_t());
    query->bindValue(":state", state);

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
    }
    return;
}
// Supprime un utilisateur de la DB.
void Database::removeUserByID(int id)
{
    query->prepare("DELETE FROM users WHERE id=:id;");

    query->bindValue(":id", id);

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
    }
    return;
}


/**********************************************/
/*************** TAG MANAGEMENT ***************/
/**********************************************/

// Obtien sous forme de modèle la liste des tags visible.
QSqlQueryModel *Database::getTagsModel(bool debug)
{
    if(!db.isOpen()) return new QSqlQueryModel;
    QString separator= getParameterValue("separator");

    emit StatusChanged(Status::Refreshing);
    QSqlQueryModel *queryModel = new QSqlQueryModel;

    if(debug)
        query->prepare("SELECT * FROM tags "
                       "ORDER BY id;");
    else
        query->prepare("SELECT "
                     " id AS Id, "
                     " value AS Value, "
                     " REPLACE(REPLACE(mixed, '</TAG>', :separator), '<TAG>', :separator) AS 'Mixed'"
                     " FROM "
                     " tags "
                     " WHERE "
                     " isActive=1 "
                       "ORDER BY id;");

    query->bindValue(":separator", separator);

    // Execute la requête
    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return queryModel;
    }

    queryModel->setQuery(*query);
    emit StatusChanged(Status::Connected);
    return queryModel;
}
// Obtien un tag à partir de son ID.
TagInfo Database::getTagByID(int id)
{
    QString separator= getParameterValue("separator");
    TagInfo tag;
    if(!db.isOpen()) return tag;

    query->prepare("SELECT"
                   " id,"
                   " value,"
                   " REPLACE(REPLACE(mixed, '</TAG>', :separator), '<TAG>', :separator),"
                   " dateAdded,"
                   " dateEdited,"
                   " dateRemoved,"
                   " isActive "
                   "FROM"
                   " tags "
                   "WHERE "
                   "id=:id;");

    query->bindValue(":id", id);
    query->bindValue(":separator", separator);

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return tag;
    }

    query->first();

    tag.id= query->value(0).toInt();
    tag.value= query->value(1).toString();
    tag.mixed= query->value(2).toString();
    tag.dateAdded= query->value(3).toInt();
    tag.dateEdited= query->value(4).toInt();
    tag.dateRemoved= query->value(5).toInt();
    tag.isActive= query->value(6).toInt();

    return tag;
}
// Retourne une liste de tous les Tags au format QList<QList<QString>>.
QList<QStringList> Database::getTags()
{
    QString separator= getParameterValue("separator");
    QList<QStringList> tags;
    if(!db.isOpen()) return tags;

    query->prepare("SELECT"
                   " id,"
                   " value,"
                   " REPLACE(REPLACE(mixed, '</TAG>', :separator), '<TAG>', :separator),"
                   " dateAdded,"
                   " dateEdited,"
                   " dateRemoved,"
                   " isActive "
                   "FROM"
                   " tags "
                   "WHERE "
                   "isActive=1 "
                   "ORDER BY id;");

    query->bindValue(":separator", separator);

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return tags;
    }

    while(query->next())
    {
        QList<QString> tag;

        for(int i= 0; i< query->record().count(); i++) {
            tag.append(query->value(i).toString());
        }

        tags.append(tag);
    }

    emit StatusChanged(Status::Connected);
    return tags;
}
// Crée ou modifie un tag.
void Database::setTag(TagInfo tag)
{
    if(!db.isOpen()) return;

    if(tag.id==-1) {
    // Ajoute un nouvel enregistrement.
        qDebug() << "Create";
        query->prepare("INSERT INTO tags ("
                      "value,mixed,dateAdded,dateEdited,isActive"
                      ")VALUES ("
                      ":value,"
                      ":mixed,"
                      ":dateAdded,"
                      ":dateEdited,"
                      ":isActive"
                      ");"
                     );
    }else{
    // Edite un enregistrement existant.
        qDebug() << "Edit";
        query->prepare("UPDATE tags SET "
                       " value=:value,"
                       " mixed=:mixed,"
                       " dateEdited=:dateEdited,"
                       " isActive=:isActive "
                       "WHERE id=:id;"
                     );
    }

    // Rempli les champs avec les données.
    query->bindValue(":value",      tag.value);
    query->bindValue(":mixed",      tag.mixed);
    query->bindValue(":dateAdded",  tag.dateAdded);
    query->bindValue(":dateEdited", tag.dateEdited);
    query->bindValue(":isActive",   tag.isActive);
    query->bindValue(":id",         tag.id);

    // Execute la requête.
    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
    }
    return;
}
// Change l'état d'un tag.
void Database::changeTagState(int id, int state)
{
    if(!db.isOpen()) return;

    query->prepare("UPDATE tags SET "
                   " isActive=:state, "
                   " dateRemoved=:dateRemoved "
                   "WHERE id=:id;"
                 );

    query->bindValue(":id", id);
    query->bindValue(":dateRemoved", QDateTime::currentDateTime().toTime_t());
    query->bindValue(":state", state);

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
    }
    return;
}
// Supprime un tag de la DB.
void Database::removeTagByID(int id)
{
    query->prepare("DELETE FROM tags WHERE id=:id;");
    query->bindValue(":id", id);

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
    }
    return;
}


/**********************************************/
/************ COMPANY MANAGEMENT **************/
/**********************************************/

// Obtien la liste des tous les champs company.
QList<QString> Database::getCompanyTags(QString separator)
{
    QList<QString> data;
    if(!db.isOpen()) return data;

    query->prepare("PRAGMA table_info(users);");

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return data;
    }

    while(query->next())
        data.append("comp_" + query->value(1).toString());

    return data;
}
// Obtien les informations de la companie.
UserInfo Database::getCompany()
{
    UserInfo user;
    if(!db.isOpen()) return user;

    query->prepare("SELECT * FROM users WHERE isCompany=1 "
                   "ORDER BY id;");

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return user;
    }
    query->first();

    user.id         = query->value(0).toInt();
    user.folder     = query->value(1).toString();
    user.filename   = query->value(2).toString();
    user.name       = query->value(3).toString();
    user.function   = query->value(4).toString();
    user.email      = query->value(5).toString();
    user.phone1     = query->value(6).toString();
    user.phone2     = query->value(7).toString();
    user.image      = query->value(8).toString();
    user.dateAdded  = query->value(9).toInt();
    user.dateEdited = query->value(10).toInt();
    user.dateRemoved= query->value(11).toInt();
    user.isCompany  = query->value(12).toInt();
    user.isActive   = query->value(13).toInt();

    return user;
}
// Obtien les informations de l'a compagnie'entreprise.
QList<QString> Database::getCompanyList()
{
    QList<QString> user;
    if(!db.isOpen()) return user;

    query->prepare("SELECT * FROM users WHERE isCompany=1 "
                   "ORDER BY id;");

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return user;
    }

    query->first();

    for(int i= 0; i< query->record().count(); i++) {
        user.append(query->value(i).toString());
    }

    return user;
}


/**********************************************/
/**************** PARAMETRERS *****************/
/**********************************************/

// Obtien la valeur d'un paramètre.
QString Database::getParameterValue(QString name)
{
    if(!db.isOpen()) return QString::null;

    query->prepare("SELECT value FROM parameters WHERE name = :name "
                   "ORDER BY id;");
    query->bindValue(":name", name);

    if(!query->exec()) {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
    }

    query->first();
    return query->value(0).toString();
}
void Database::setParameterValue(QString name, QString value)
{
    if(!db.isOpen()) return;

    query->prepare("UPDATE parameters SET "
                   " value=:value "
                   "WHERE name=:name;"
                 );

    query->bindValue(":name", name);
    query->bindValue(":value", value);

    if(!query->exec())
    {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
    }

    return;
}
// Obtien le titre des colonnes de la table users.
QList<QString> Database::getColsName()
{
    QList<QString> cols;
    if(!db.isOpen()) return cols;

    query->prepare("PRAGMA table_info(users);");

    if(!query->exec())
    {
        lastError= query->lastError().text();
        DEBUGINFO + query->lastError().text();
        emit StatusChanged(Status::Error);
        return cols;
    }

    while(query->next())
        cols.append(query->value(1).toString());

    return cols;
}
