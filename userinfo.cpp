#include "userinfo.h"

UserInfo::UserInfo(int id, QString folder, QString filename, QString name, QString function,
                   QString email, QString phone1, QString phone2, QString image,
                   int dateAdded, int dateEdited, int dateRemoved, int isActive, int isCompany)
{
    this->id= id;
    this->folder= folder;
    this->filename= filename;
    this->name= name;
    this->function= function;
    this->email= email;
    this->phone1= phone1;
    this->phone2= phone2;
    this->image= image;
    this->dateAdded= dateAdded;
    this->dateEdited= dateEdited;
    this->dateRemoved= dateRemoved;
    this->isActive= isActive;
    this->isCompany= isCompany;
}

UserInfo::~UserInfo() {}

bool UserInfo::checkFields() {
    lastError="";

    // Vérifie que le folder ne contienne pas d'espaces.
    if(folder.contains(" "))
        lastError+= "Folder cannot contain spaces.\n";

    // Vérifie si le champ nom est vide.
    if(filename == QString::null || filename.trimmed() == "")
        lastError+= "LastName/Firstname cannot be empty.\n";

    // Vérifie si le champ fonction est vide.
    if(function == "")
        lastError+= "Funcion cannot be empty.\n";

    // Vérifie le format de l'email.
    QRegExp emailExp("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    emailExp.setCaseSensitivity(Qt::CaseInsensitive);
    emailExp.setPatternSyntax(QRegExp::RegExp);

    if(!emailExp.exactMatch(email))
        lastError+= "Invalid email format.\n";


    // Vérifie le format du téléphone 1.
    QRegExp phoneExp("^\\+\\d{2} (\\d+| )+$"); //^\\+32([/|\\.| ][\\d]{2}){4}$
    phoneExp.setCaseSensitivity(Qt::CaseInsensitive);
    phoneExp.setPatternSyntax(QRegExp::RegExp);
    if(phone1.trimmed()!="")
    {
        if(phoneExp.indexIn(phone1) == -1)
            lastError+= "Invalid Phone1 format.\n";

        // Vérifie le format du téléphone 2.
        if(phone2!="" && phoneExp.indexIn(phone2) == -1)
            lastError+= "Invalid Phone2 format.\n";
    }

    // Vérifie si l'image est valide.
    QFileInfo imageInfo(image);

    if(image.trimmed()!="")
        if(!imageInfo.isFile())
            lastError+= "Image is not a file.\n";


    // Vérifie si il n'y à pas d'erreur(s).
    if(lastError!="") {
        return false;
    }

    return true;
}
