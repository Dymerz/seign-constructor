#ifndef DATABASE_H
#define DATABASE_H
#define DEBUGINFO qDebug() << "You are in" << __FILE__ << __FUNCTION__ << "line" << __LINE__ << " "

#include <QObject>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlResult>
#include <QSqlRecord>
#include <QSqlError>
#include <QFileInfo>
#include <QListView>
#include <QList>
#include <QDateTime>
#include <QMetaEnum>

#include <settings.h>
#include <userinfo.h>
#include <taginfo.h>

class Database : public QObject
{
    Q_OBJECT

public:
    enum Status
    { Connected, Disconnected, Refreshing, Error, Unknown, ConnectinError, PathError, BadToken};
    Q_ENUM(Status)

    enum TagType
    { DB, Company, Other, None };
    Q_ENUM(TagType)

    explicit Database(QObject *parent = 0);
    ~Database();

    void startConnect();
    void clean();
    bool isValid();

    QSqlQueryModel *getUsersModel(bool debug= false);
    UserInfo getUserByID(int id);
    QList<QStringList> getUsersList();
    QList<QString> getCompanyTags(QString separator);
    void setUser(UserInfo user);
    void changeUserState(int id, int state);
    void removeUserByID(int id);

    UserInfo getCompany();
    QList<QString> getCompanyList();
//    void setCompany(UserInfo company);

    QSqlQueryModel *getTagsModel(bool debug= false);
    TagInfo getTagByID(int id);
    QList<QStringList> getTags();
    void setTag(TagInfo tag);
    void changeTagState(int id, int state);
    void removeTagByID(int id);

    QString getParameterValue(QString name);
    void setParameterValue(QString name, QString value);
    QList<QString> getColsName();

    QString lastError;

private:
    QSqlDatabase db;
    QSqlQuery *query= new QSqlQuery;
    Settings file;

signals:
    void StatusChanged(Database::Status state);
};

#endif // DATABASE_H
