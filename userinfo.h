#ifndef USERINFO_H
#define USERINFO_H

#include <QString>
#include <QDebug>
#include <QRegExp>
#include <QFileInfo>
#include <QList>

class UserInfo
{

public:
    explicit UserInfo(int id= -1,
                      QString folder="",
                      QString name="",
                      QString filename="",
                      QString function="",
                      QString email="",
                      QString phone1="",
                      QString phone2="",
                      QString image="",
                      int dateAdded= 0,
                      int dateEdited= 0,
                      int dateRemoved= 0,
                      int isActive= 0,
                      int isCompany= 0
            );
    ~UserInfo();

    bool checkFields();

    int id          = -1;
    QString folder  = "";
    QString filename= "";
    QString name    = "";
    QString function= "";
    QString email   = "";
    QString phone1  = "";
    QString phone2  = "";
    QString image   = "";
    int dateAdded   = 0;
    int dateEdited  = 0;
    int dateRemoved = 0;
    int isActive    = 0;
    int isCompany   = 0;

    QString lastError;


private:
};

#endif // USERINFO_H

