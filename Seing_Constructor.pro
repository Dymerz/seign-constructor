#-------------------------------------------------
#
# Project created by QtCreator 2017-03-16T12:45:10
#
#-------------------------------------------------

QT       += core gui widgets sql gui

TARGET = Seing_Constructor
TEMPLATE = app

VERSION = 0.5.3

TRANSLATIONS += Seing_Constructor_fr_be.ts Seing_Constructor_en_us.ts Seing_Constructor_fr_fr.ts
INCLUDEPATH += views
DEPENDPATH += views

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
    database.cpp \
    userinfo.cpp \
    taginfo.cpp \
    settings.cpp \
    views/tageditor.cpp \
    views/parameters.cpp \
    views/mainwindow.cpp \
    views/usereditor.cpp \
    mylineedit.cpp \
    generate.cpp \
    file.cpp \
    highlighter.cpp \
    views/about.cpp

HEADERS  += database.h \
    userinfo.h \
    taginfo.h \
    settings.h \
    views/tageditor.h \
    views/parameters.h \
    views/mainwindow.h \
    views/usereditor.h \
    mylineedit.h \
    generate.h \
    file.h \
    highlighter.h \
    views/about.h

FORMS    += views/tageditor.ui \
    views/parameters.ui \
    views/mainwindow.ui \
    views/usereditor.ui \
    views/about.ui

RC_ICONS = icon.ico

DISTFILES +=
