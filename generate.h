#ifndef GENERATE_H
#define GENERATE_H

#include <QObject>
#include <QDebug>
#include <QProgressDialog>
#include <QList>
#include <QMap>
#include <QThread>
#include <QFileInfo>

#include <database.h>
#include <userinfo.h>
#include <taginfo.h>
#include <file.h>

class Generate : public QObject
{
    Q_OBJECT
public:
    explicit Generate(QWidget *mainwindow, Database *db, Settings *options, QObject *parent = 0);

    void start(QList<QStringList> users, QString dest="");
    void initDialog();
    void loadData(QList<QStringList> users);

    void startReplace();
    QString replaceTags(QList<QString> &user);

    bool checkData();
    bool checkFiles();
    bool checkDestination();

signals:
    void ProgressChanged(int i);

private:
    Database *db;
    QWidget *mainwindow;
    Settings *options;

    QProgressDialog *popupProgress; // Popup de progression.

    QList<QStringList> users; // liste des informations utilisateurs.
    QList<QStringList> tags; // liste des informations des tags
    QList<QString> company; // informations de la societé.
    QList<QString> colsName; // Nom des colonnes des users.
    QString htmlModal; // contenu à parser du fichier htm.

    QString separator; // contain the separator chain.
    QString usersPath; // contain the user destination path.
    QString imagesPath; // contain the images destination path.
    QString htmlPath; // contain the html modal path.
    QString motd; // contain the general message.

    QString dest;
public slots:

};

#endif // GENERATE_H
