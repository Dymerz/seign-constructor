#include "file.h"

File::File(QObject *parent) : QObject(parent)
{}

// Read to file.
QString File::read(QString path)
{
    QFile file(path);

    if(!file.open(QIODevice::ReadOnly))
        qDebug() << "HTML modele not found!";

    QTextStream instream(&file);
    QString text= instream.readAll();

    file.close();
    return text;
}
// Write to file.
void File::write(QString path, QString text)
{
    QFile file(path);

    if(!file.open(QIODevice::WriteOnly))
        qDebug() << "file error";

    QTextStream outstream(&file);
    outstream << text;
    file.close();

    return;
}
// Write to file and create dir.
QString File::write(QString path, QString filename, QString text, bool indexing)
{
    QDir dir;
    QFile *file;

    dir = QDir(path);
    dir.mkpath(path);

    if(dir.count()==2 || !indexing)
        file= new QFile(QString("%1/%2.htm").arg(path).arg(filename));
    else
        file= new QFile(QString("%1/%2_%3.htm").arg(path).arg(filename).arg(dir.count()-2));

    if(!file->open(QIODevice::WriteOnly))
        qDebug() << "file error";

    QTextStream outstream(file);
    outstream << text;
    file->close();
    return QFileInfo(file->fileName()).baseName();
}
// Create directory.
void File::createDir(QString path)
{
    QDir dir;
    dir.mkpath(path);
    return;
}
// Copy image file to destination.
void File::copy(QString src, QString dst)
{
    QFileInfo source(src);
    QString ext= source.fileName().split(".").at(1);

    QFile::copy(src, QString("%1.%2").arg(dst).arg(ext));
    return;
}
// Rename folder by adding .old
void File::backupDir(QString src, QString dst)
{
    QDir dir;
    dir.rename(src, dst);
    return;
}
// Remove a folder and all files.
void File::clean(QString folder)
{
    QDir dir(folder);
    dir.removeRecursively();
    return;
}
// Supprime un fichier.
bool File::remove(QString path)
{
    QFile file(path);
    return file.remove();
}
// Check if file exist.
bool File::exist(QString path)
{
    QFile file(path);
    return file.exists();
}
// Compare two file's content.
bool File::equal(QString first, QString second)
{
    QString source= read(first);
    QString compare= read(second);

    return source == compare;
}
