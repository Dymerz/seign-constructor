#include "mylineedit.h"

MyLineEdit::MyLineEdit(Database *db, QWidget *parent)
{
    separator= db->getParameterValue("separator");
    return;
}

void MyLineEdit::dragEnterEvent(QDragEnterEvent *e)
{
    if (e->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist")) {
            e->acceptProposedAction();
    }
    return;
}

void MyLineEdit::dragMoveEvent(QDragMoveEvent *e)
{

    return;
}

void MyLineEdit::dropEvent(QDropEvent *e)
{
    const QMimeData *mimeData = e->mimeData();

    QStandardItemModel model;
    model.dropMimeData(e->mimeData(), Qt::CopyAction, 0,0, QModelIndex());
    QString item = model.index(0,0).data().toString();

    this->cursorPosition();
    this->insert(separator+item+separator);
    return;
}
