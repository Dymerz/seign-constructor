#ifndef FILE_H
#define FILE_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QTextStream>

#include <QDebug>

class File : public QObject
{
    Q_OBJECT
public:
    explicit File(QObject *parent = 0);

    static QString read(QString path);
    static void write(QString  path, QString text);
    static QString write(QString  path, QString filename, QString text, bool indexing= true);
    static void createDir(QString path);
    static void copy(QString src, QString dst);
    static void backupDir(QString src, QString dst);
    static void clean(QString folder);
    static bool remove(QString path);
    static bool exist(QString path);
    static bool equal(QString first, QString second);
signals:

public slots:
};

#endif // FILE_H
