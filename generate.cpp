#include "generate.h"

Generate::Generate(QWidget *mainwindow, Database *db, Settings *options, QObject *parent) : QObject(parent)
{
    this->mainwindow= mainwindow;
    this->db = db;
    this->options= options;
}

void Generate::start(QList<QStringList> users, QString dest)
{
    this->dest= dest;

    loadData(users);
    initDialog();
    startReplace();
    return;
}

void Generate::initDialog()
{
    QString dialogTitle;

    dialogTitle= QString("Generating %1 user(s)").arg(users.length());

    popupProgress= new QProgressDialog(dialogTitle, "Abord", 1, users.length(), mainwindow);
    popupProgress->setWindowModality(Qt::WindowModal);
    popupProgress->show();
    return;
}

void Generate::loadData(QList<QStringList> users)
{
    separator= db->getParameterValue("separator");
    usersPath= (dest!="" ? dest : db->getParameterValue("usersPath"));
    htmlPath= db->getParameterValue("htmlPath");
    motd= db->getParameterValue("motd");

    tags= db->getTags();
    colsName= db->getColsName();
    this->users= users;
    company= db->getCompanyList();

    htmlModal= File::read(htmlPath);
    return;
}

void Generate::startReplace()
{
    if(dest=="")
    {
        File::backupDir(usersPath, usersPath + ".old"); // Crée un backup le dossier
        File::clean(usersPath); // Supprime tous les fichiers utilisateurs
    }

    File::createDir(usersPath);
    File::createDir(usersPath + "/[images]");

    foreach(QList<QString> user, users) {
        QString filename= File::write(usersPath + "\\" + user[1], user[2], replaceTags(user));
        if(user[8] !=QString::null && user[8] != "")
            File::copy(user[8], QString("%1/[images]/%2").arg(usersPath).arg(filename));

        popupProgress->setValue(popupProgress->value()+1);
    }
    popupProgress->setValue(users.length());
    popupProgress->close();
    return;
}

// Remplace toutes les variables des Tags.
QString Generate::replaceTags(QList<QString> &user)
{
    QList<QList<QString>> others;   // Liste des variables Others.
    QString htmlResult= htmlModal;  // Place le modèle HTML dans la variable locale pour le traitement.

    // Ajoute les éléments suivant dans la liste Others.
    QDateTime date= QDateTime::currentDateTime();
    others.append(QList<QString>() << "time" << date.toString("hh:mm:ss"));
    others.append(QList<QString>() << "hours" << date.toString("hh"));
    others.append(QList<QString>() << "minutes" << date.toString("mm"));
    others.append(QList<QString>() << "seconds" << date.toString("ss"));

    others.append(QList<QString>() << "date" << date.toString("dd/MM/yyyy"));
    others.append(QList<QString>() << "day" << date.toString("dd"));
    others.append(QList<QString>() << "month" << date.toString("MM"));
    others.append(QList<QString>() << "year" << date.toString("yyyy"));

    others.append(QList<QString>() << "motd" << motd);

    if(user[8].trimmed() == "")
        user[8]=company[8];

    QFileInfo file(user[8]);
    others.append(QList<QString>() << "FILENAME_OF_IMAGE" << (user[2] + "." + file.completeSuffix()));

    // Traitement et remplacement des Tags.
    foreach(QList<QString> tag, tags)
    {
        foreach(QList<QString> other, others)
            tag[2]= tag[2].replace(separator+other[0]+separator, other[1]); // Remplace les tags Others.

        for(int i= 0; i< colsName.length(); i++)
            tag[2]= tag[2].replace(separator+colsName[i]+separator, user[i]); // Remplace les tags User.

        for(int i= 0; i< colsName.length(); i++)
            tag[2]= tag[2].replace(separator+"comp_"+colsName[i]+separator, company[i]); // Remplace les tags Company.

        htmlResult= htmlResult.replace(separator+tag[1]+separator, tag[2]); // Écrit les changements dans la variable de résultat.
    }

    return htmlResult;
}

/**********************************************/
/*************CHECKING FUNCTIONS **************/
/**********************************************/

bool Generate::checkData()
{

}

bool Generate::checkFiles()
{

}

bool Generate::checkDestination()
{

}
