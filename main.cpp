#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
/**
 * @file main.cpp
 * @brief Programme de génération de signatures HTM.
 * @author Urbain.C
 * @version 0.5.2
 * @date 31 March 2017
 */

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator qTranslator;

    qTranslator.load(QCoreApplication::applicationDirPath()+"/Seing_Constructor_fr_be.qm");
    a.installTranslator(&qTranslator);

    MainWindow w;
    w.show();

    return a.exec();
}
