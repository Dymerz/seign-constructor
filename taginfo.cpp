#include "taginfo.h"

TagInfo::TagInfo()
{
    others << "time" << "date" << "day" << "month" << "year" << "hours" << "minutes" << "seconds" << "motd" << "FILENAME_OF_IMAGE";
}

QList<QString> TagInfo::extractTagsFromText(QString text, QString separator)
{
    QRegExp variableExp(separator+"[A-Za-z0-9|_]+"+separator);

    QList<QString> findTags;

    int pos = 0;
    while ((pos = variableExp.indexIn(text, pos)) != -1) {
         findTags << variableExp.cap(0).mid(separator.length(), variableExp.cap(0).length() - separator.length()-1);
         pos += variableExp.matchedLength();
     }

    return findTags;
}

// Format de mixed to be stocked to the DB
void TagInfo::prepareToDB(QString separator)
{
    QRegExp variableExp(separator+"[A-Za-z0-9|_]+"+separator);
    if(!isTagFormated || !isMixedFormated)
    {
        qDebug() << "Tag or Mixed field aren't validated, call checkTagSyntax and checkMixedSyntax before run this function.";
        return;
    }

    int pos = 0;
    while ((pos = variableExp.indexIn(mixed, pos)) != -1) {
        mixed= mixed.replace(pos + variableExp.matchedLength()-1, separator.length(), "</TAG>");
        mixed= mixed.replace(pos, separator.length(), "<TAG>");

        pos += variableExp.matchedLength();
     }
    return;
}

bool TagInfo::checkTagSyntax(QString separator)
{
    QRegExp exp("^"+separator+"[A-Za-z0-9|_]+"+separator+"$");
    exp.setCaseSensitivity(Qt::CaseInsensitive);
    exp.setPatternSyntax(QRegExp::RegExp);

    // Vérifie la syntaxe de tag.
    if(!exp.exactMatch(value))
        return false;

    isTagFormated= true;
    return true;
}

// Vérifie l'existence des tags.
bool TagInfo::checkMixedSyntax(QString separator, QStringList tagsList)
{
    QList<QString> extractedTags= extractTagsFromText(mixed, separator);
    bool result= tagsExist(extractedTags, tagsList);
    isMixedFormated= result;
    return result;
}

// Vérifie l'existance de mots réservé.
bool TagInfo::checkRestrictedWord()
{
    return !(mixed.contains("<TAG>") || mixed.contains("</TAG>"));
}

bool TagInfo::tagsExist(QStringList search, QStringList container)
{
    int match= 0;
    QList<QString>::iterator i;

    for(i= search.begin(); i != search.end(); i++)
    {
        if(container.contains(*i))
            match++;
        else
            qDebug() << "field " + *i + " does't not exist";
    }

    // si le nombre de tags trouvé correspond au nombre de tags  alors ils existent tous.
    if(match== search.length())
        return true;
    return false;
}

