#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QFile>
#include <QTextStream>
#include <QDebug>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);

public:
//    Functions
    QString getDBPath() const;
    void setDBPath(QString path);

    bool getDebugMode() const;
    void setDebugMode(bool val);

private:
//    Vars
    QSettings *option;
};

#endif // SETTINGS_H
