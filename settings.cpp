#include "settings.h"

// Constructeur pour initialiser les paramètres de QSettings
Settings::Settings(QObject *parent) : QObject(parent)
{
    // initialise l'objet "option"
    option= new QSettings("Atelier Cambier", "Seign_Constructor");
}

// Retourne l'emplacement de la DB via le registre.
QString Settings::getDBPath() const
{
    return option->value("DBPath", "NULL").toString();
}

// Définis l'emplacement de la DB dans le registre.
void Settings::setDBPath(QString path)
{
    option->setValue("DBPath", path);
    return;
}

// Retourne la valeur de Debug via le registre.
bool Settings::getDebugMode() const
{
    return option->value("Debug", "0").toBool();
}

// Définis laa valeur de Debug via le registre.
void Settings::setDebugMode(bool val)
{
    option->setValue("Debug", val);
    return;
}
